-- liquibase formatted sql
        
-- changeset shad:cf1271e0-4352-4773-9aba-81577081208d
create table t_user (
    id uuid constraint pk_t_user primary key,
    email varchar
        constraint nn_t_user_email not null,
    normalised_email varchar
        constraint nn_t_user_normalised_email not null
        constraint unq_t_user_normalised_email unique,
    password_hash varchar
        constraint nn_t_user_password_hash not null,
    date_creation timestamp with time zone
        constraint nn_t_user_date_creation not null,
    is_active boolean
        constraint nn_t_user_is_active not null
        constraint df_t_user_is_active default false,
    validation_token uuid
        constraint unq_t_user_validation_token unique,
    validation_token_expiration_date timestamp with time zone
);
-- rollback drop table t_user;
