#[actix_web::test]
async fn test_get_user_by_email_ok() {
    // setup
    let (_, repos, services) = crate::test::setup().await;
    // arrange
    let all_users = crate::test::get_users();
    let expected_user = all_users.first().unwrap();
    let expected_user_json = serde_json::to_string(&super::super::UserOutput::from(expected_user.clone())).unwrap();
    let path = actix_web::web::Path::from(super::GetUserByEmailUrlParams { email: expected_user.email.clone() });
    // act
    let actual_user_json_result = super
        ::route(
            actix_web::web::Data::new(repos),
            actix_web::web::Data::new(services),
            path)
        .await;
    // assert
    assert!(actual_user_json_result.is_ok());
    assert_eq!(actual_user_json_result.unwrap(), expected_user_json);
    // cleanup
    crate::test::cleanup().await;
}

#[actix_web::test]
async fn test_get_user_by_email_ko() {
    // setup
    let (_, repos, services) = crate::test::setup().await;
    // arrange
    let path = actix_web::web::Path::from(
        super::GetUserByEmailUrlParams { email: String::from("nonexisting@gmail.com") }
    );
    // act
    let actual_user_json_result = super
        ::route(
            actix_web::web::Data::new(repos),
            actix_web::web::Data::new(services),
            path)
        .await;
    // assert
    assert!(actual_user_json_result.is_err());
    // cleanup
    crate::test::cleanup().await;
}
