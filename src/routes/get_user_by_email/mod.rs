#[cfg(test)]
mod tests;

#[derive(serde::Deserialize)]
pub(super) struct GetUserByEmailUrlParams {
    email: String,
}

pub(super) async fn route(
    repositories: actix_web::web::Data<crate::Repositories>,
    services: actix_web::web::Data<crate::Services>,
    url_params: actix_web::web::Path<GetUserByEmailUrlParams>
) -> Result<String, crate::AppError> {
    let email = services.emails_service().check_and_normalise_email(&url_params.email)?;
    let users = repositories
        .user_repository()
        .read(None, Some(vec![ email ]))
        .await?
        .into_iter()
        .map(|x| x.into())
        .collect::<Vec<super::UserOutput>>();
    let user = users
        .first()
        .ok_or(crate::AppError { 
            http_status_code: actix_web::http::StatusCode::BAD_REQUEST,
            message: "Invalid email".into(),
            server_status_code: crate::ServerStatusCode::ProvidedEmailDoesNotExist,
        })?;
    let user_json = serde_json::to_string(&user)?;
    Ok(user_json)
}
