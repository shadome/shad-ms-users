#[cfg(test)]
mod tests;

#[derive(Clone, serde::Deserialize)]
pub(super) struct RegisterNewUserPayload {
    email: String,
    password: String,
}

#[derive(Clone, Copy, serde::Deserialize, serde::Serialize)]
pub(super) struct RegisterNewUserOutput {
    user_id: uuid::Uuid,
    validation_token: uuid::Uuid,
    validation_token_expiration_date: chrono::DateTime<chrono::Utc>,
}

const MAX_NB_OF_ACCOUNTS: u8 = 10;

// Example of command for integration testing:
// curl -i -X POST -H 'Content-Type: application/json' -d '{"email": "toto@gmail.com", "password": "toto"}' http://localhost:8090/users/register
pub(super) async fn route(
    repositories: actix_web::web::Data<crate::Repositories>,
    services: actix_web::web::Data<crate::Services>,
    post_payload: actix_web::web::Json<RegisterNewUserPayload>,
) -> Result<String, crate::AppError> {
    let normalised_email = services
        .emails_service()
        .check_and_normalise_email(&post_payload.email)?;
    let password_hash = services
        .crypto_service()
        .hash_password(secrecy::SecretString::from(post_payload.password.clone()))?;
    ensure_provided_email_does_not_exist(repositories.get_ref(), &normalised_email).await?;
    ensure_account_limit_is_not_reached(repositories.get_ref()).await?;
    let user_id = 
        register_new_unconfirmed_user(
            repositories.get_ref(),
            &post_payload.email,
            &normalised_email,
            &password_hash)
        .await?;
    Ok(user_id.to_string())
}

async fn ensure_account_limit_is_not_reached(
    repositories: &crate::Repositories,
) -> Result<(), crate::AppError> {
    let current_account_nb = repositories
        .user_repository()
        .count()
        .await?;
    if current_account_nb.as_u32() as u8 >= MAX_NB_OF_ACCOUNTS{
        let error = crate::AppError{ 
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: crate::ServerStatusCode::MaxNbOfAccountsReached,
            message: String::from("Maximum number of account reached"),
        };
        return Err(error);
    }
    Ok(())
}

async fn ensure_provided_email_does_not_exist(
    repositories: &crate::Repositories,
    normalised_email: &str,
) -> Result<(), crate::AppError> {
    let users = repositories
        .user_repository()
        .read(None, Some(vec![ normalised_email.to_owned() ]))
        .await?;
    if users.len() > 0 {
        let error = crate::AppError {
            http_status_code: actix_web::http::StatusCode::BAD_REQUEST,
            server_status_code: crate::ServerStatusCode::EmailAlreadyExists,
            message: String::from("An account for this email already exists"),
        };
        return Err(error);
    } 
    Ok(())
}

async fn register_new_unconfirmed_user(
    repositories: &crate::Repositories,
    email: &str,
    normalised_email: &str,
    password_hash: &str,
) -> Result<uuid::Uuid, crate::AppError> {
    let user_id = repositories
        .user_repository()
        .create(email, normalised_email, password_hash)
        .await?;
    Ok(user_id)
}
