// mod confirm_registration;
mod get_all_users;
mod get_user_by_email;
mod register_new_user;

pub(super) async fn run_webapp(
    env: crate::Env,
    services: crate::Services,
    repos: crate::Repositories,
) -> Result<(), crate::AppError> {
    let app_closure = move || {
        actix_web::App::new()
            // avoid generic http pages returned in case of internal panic
            .wrap(actix_web::middleware::ErrorHandlers::new())
            // enables logging in the console in which the server runs through the actix middleware
            .wrap(actix_web::middleware::Logger::default())
            .app_data(actix_web::web::Data::new(services.clone()))
            .app_data(actix_web::web::Data::new(repos.clone()))
            .route("/register", actix_web::web::post().to(register_new_user::route))
            .route("/users", actix_web::web::get().to(get_all_users::route))
            .service(
                actix_web::web::scope("/users")
                    .route("/{email}", actix_web::web::get().to(get_user_by_email::route))
            )
    };

    // the following should never stop looping
    actix_web::HttpServer::new(app_closure)
        .bind((env.host.as_str(), env.port))?
        // .bind_openssl((env.host.as_str(), env.port), builder)?
        .run()
        .await?;
    Ok(())
}

// Note: even though the XxxOutput struct may be the same as the Xxx struct,
// it is still userful for ensuring that a future modification to the Xxx struct in the repositories layer
// will not induce silent and potentially breaking side effects to APIs
#[derive(serde::Serialize)]
struct UserOutput {
    pub(self) id: uuid::Uuid,
    pub(self) email: String,
    pub(self) date_creation: chrono::DateTime<chrono::Utc>,
    pub(self) is_active: bool,
    pub(self) validation_token: Option<uuid::Uuid>,
    pub(self) validation_token_expiration_date: Option<chrono::DateTime<chrono::Utc>>,
}

impl From<crate::User> for UserOutput {
    fn from(user: crate::User) -> Self {
        UserOutput {
            id: user.id,
            email: user.email,
            date_creation: user.date_creation,
            is_active: user.is_active,
            validation_token: user.validation_token,
            validation_token_expiration_date: user.validation_token_expiration_date,
        }
    }
}

#[cfg(test)]
mod tests {

    // Note that neither OK nor KO tests are meant to extensively re-test
    // the crates which are used, only a few happy / sad paths.
    
    // #[actix_web::test]
    // async fn test_check_and_normalise_email_ok() {
    //     // arrange
    //     let email = "toto@gmail.com";
    //     let expected = String::from(email);
    //     // act
    //     let result = super::check_and_normalise_email(email).await;
    //     // assert
    //     assert!(result.is_ok());
    //     assert_eq!(result.unwrap(), expected);
    // }

    // /// Check that the email subparts are correctly ignored when normalisation occurs
    // #[test]
    // fn test_check_and_normalise_email_ok_subparts() {
    //     // arrange
    //     let email = "t.O.T.O+submailpart+anothersubmailpart@GMAIL.com";
    //     let expected = String::from("toto@gmail.com");
    //     // act
    //     let result = super::check_and_normalise_email(email);
    //     // assert
    //     assert!(result.is_ok());
    //     assert_eq!(result.unwrap(), expected);
    // }

    // /// Check that unknown providers are not refused by default, i.e.,
    // /// this function uses a blacklist, not a whitelist. <br/>
    // /// From <a>https://www.randomlists.com/email-addresses</a>
    // #[test]
    // fn test_check_and_normalise_email_ok_not_blacklisted() {
    //     // arrange
    //     let emails = vec![
    //         "choset@live.com",
    //         "arathi@icloud.com",
    //         "hamilton@msn.com",
    //         "mcast@hotmail.com",
    //         "research@att.net",
    //         "gerlo@me.com",
    //         "calin@yahoo.com",
    //         "sblack@sbcglobal.net",
    //         "presoff@optonline.net",
    //         "bogjobber@gmail.com",
    //         "muadip@comcast.net",
    //         "duncand@outlook.com",
    //         "garland@aol.com",
    //         "zwood@mac.com",
    //         "john.doe@externe.e-i.com", // random french company domain
    //     ];
    //     // act, assert
    //     for email in emails {
    //         let result = super::check_and_normalise_email(email);
    //         assert!(result.is_ok());
    //         assert_eq!(result.unwrap(), email);
    //     }
    // }

    // #[test]
    // fn test_check_and_normalise_email_ko_invalid_1() {
    //     // arrange
    //     let email = "toto_at_gmail.com";
    //     let expected_http_status_code = actix_web::http::StatusCode::BAD_REQUEST;
    //     let expected_server_status_code = crate::ServerStatusCode::InvalidEmailFormat;
    //     // act
    //     let result = super::check_and_normalise_email(email);
    //     // assert
    //     assert!(result.is_err());
    //     let crate::AppError { http_status_code: actual_http_status_code, server_status_code: actual_server_status_code, .. } = result.unwrap_err();
    //     assert_eq!(expected_http_status_code, actual_http_status_code);
    //     assert_eq!(expected_server_status_code, actual_server_status_code);
    // }

    // #[test]
    // fn test_check_and_normalise_email_ko_invalid_2() {
    //     // arrange
    //     let email = "toto@gmail_dot_com";
    //     let expected_http_status_code = actix_web::http::StatusCode::BAD_REQUEST;
    //     let expected_server_status_code = crate::ServerStatusCode::InvalidEmailFormat;
    //     // act
    //     let result = super::check_and_normalise_email(email);
    //     // assert
    //     assert!(result.is_err());
    //     let crate::AppError { http_status_code: actual_http_status_code, server_status_code: actual_server_status_code, .. } = result.unwrap_err();
    //     assert_eq!(expected_http_status_code, actual_http_status_code);
    //     assert_eq!(expected_server_status_code, actual_server_status_code);
    // }

    // #[test]
    // fn test_check_and_normalise_email_ko_blacklisted() {
    //     // arrange
    //     let email = "toto@yopmail.com";
    //     let expected_http_status_code = actix_web::http::StatusCode::BAD_REQUEST;
    //     let expected_server_status_code = crate::ServerStatusCode::BlacklistedEmail;
    //     // act
    //     let result = super::check_and_normalise_email(email);
    //     // assert
    //     assert!(result.is_err());
    //     let crate::AppError { http_status_code: actual_http_status_code, server_status_code: actual_server_status_code, .. } = result.unwrap_err();
    //     assert_eq!(expected_http_status_code, actual_http_status_code);
    //     assert_eq!(expected_server_status_code, actual_server_status_code);
    // }

}