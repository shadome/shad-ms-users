#[cfg(test)]
mod tests;

#[derive(Clone, serde::Deserialize)]
pub(super) struct ConfirmRegistrationPayload {
    email: String,
    token: String,
}

// Example of command for integration testing:
// curl -i -X POST -H 'Content-Type: application/json' -d '{"email": "toto@gmail.com", "password": "toto"}' http://localhost:8090/users/register
pub(super) async fn route(
    repositories: actix_web::web::Data<crate::Repositories>,
    post_payload: actix_web::web::Json<ConfirmRegistrationPayload>,
) -> Result<(), crate::AppError> {
    let email = super::check_and_normalise_email(&post_payload.email)?;
    ensure_provided_email_does_not_exist(repositories.get_ref(), &email).await?;
    ensure_account_limit_is_not_reached(repositories.get_ref()).await?;
    let user_id = 
        register_new_unconfirmed_user(
            repositories.get_ref(),
            services.get_ref(),
            &email,
            &post_payload.password)
        .await?;
    Ok(user_id.to_string())
}

async fn ensure_account_is_not_already_active(
    repositories: &crate::Repositories,
    email: &str,
) -> Result<(), crate::AppError> {
    Ok(())
}

async fn ensure_email_and_token_match(
    repositories: &crate::Repositories,
    email: &str,
    validation_token: &str,
) -> Result<(), crate::AppError> {
    Ok(())
}

async fn ensure_provided_email_does_not_exist(
    repositories: &crate::Repositories,
    email: &str,
) -> Result<(), crate::AppError> {
    let users = repositories
        .user_repository()
        .read(None, Some(vec![ email.to_owned() ]))
        .await?;
    if users.len() > 0 {
        let error = crate::AppError {
            http_status_code: actix_web::http::StatusCode::BAD_REQUEST,
            server_status_code: crate::ServerStatusCode::EmailAlreadyExists,
            message: String::from("An account for this email already exists"),
        };
        return Err(error);
    } 
    Ok(())
}

async fn register_new_unconfirmed_user(
    repositories: &crate::Repositories,
    services: &crate::Services,
    email: &str,
    password: &str,
) -> Result<uuid::Uuid, crate::AppError> {
    let user_to_create = crate::User::new_creatable(
        services.crypto_service(),
        email,
        password)?;
    let user_id = repositories.user_repository().create(&user_to_create).await?;
    Ok(user_id)
}
