/// Note that this test is slow because it hashes and compares passwords.
#[actix_web::test]
async fn test_register_new_user() {
    // setup
    let (_, repos, services) = crate::test::setup().await;
    // arrange
    let expected_new_user = super::ConfirmRegistrationPayload {
        email: String::from("email@domain.com"),
        password: String::from("my_password"),
    };
    let date_before_insertion = chrono::Utc::now();
    // act
    let new_user_id = super
        ::route(
            actix_web::web::Data::new(repos),
            actix_web::web::Data::new(services.clone()),
            actix_web::web::Json(expected_new_user.clone()))
        .await
        .unwrap();
    // assert
    let returned_user_id = uuid::Uuid::parse_str(&new_user_id).unwrap();
    let user_ids_param = Some(vec![returned_user_id]);
    let actual_new_users = crate::test::read_test_users_in_db(user_ids_param.clone()).await;
    assert_eq!(actual_new_users.len(), 1);
    let actual_new_user = actual_new_users.first().unwrap();
    assert_eq!(actual_new_user.id, returned_user_id);
    assert_eq!(actual_new_user.email, expected_new_user.email);
    assert_eq!(actual_new_user.is_active, false);
    let are_passwords_matching = services
        .crypto_service()
        .compare_passwords(
            expected_new_user.password.clone().into(),
            actual_new_user.password_hash.clone())
        .unwrap();
    assert!(are_passwords_matching);
    assert!(date_before_insertion < actual_new_user.date_creation);
    assert!(chrono::Utc::now() > actual_new_user.date_creation);
    // cleanup
    crate::test::cleanup().await;
}

#[actix_web::test]
async fn test_ensure_provided_email_does_not_exist() {
    // setup
    let (_, repos, services) = crate::test::setup().await;
    // arrange
    let already_existing_email = crate::test::get_users().first().unwrap().email.clone();
    let user_to_insert = super::ConfirmRegistrationPayload {
        email: already_existing_email,
        password: String::from("my_password"),
    };
    // act
    let opt_expected_err = super
        ::route(
            actix_web::web::Data::new(repos),
            actix_web::web::Data::new(services.clone()),
            actix_web::web::Json(user_to_insert.clone()))
        .await;
    // assert
    assert!(opt_expected_err.is_err());
    let crate::AppError { http_status_code, server_status_code, message } = opt_expected_err.err().unwrap();
    assert!(http_status_code.as_u16() >= 400 && http_status_code.as_u16() < 500);
    assert!(!message.is_empty());
    assert_eq!(server_status_code, crate::ServerStatusCode::EmailAlreadyExists);
    // cleanup
    crate::test::cleanup().await;
}

/// Note that this test might prove too costly if `super::MAX_NB_OF_ACCOUNTS` gets bigger, possible solutions:
/// - bulk insert a lot of data in pure SQL to reach the limit
/// - make this number a variable, and set a smaller amount for test purposes (*)
/// (*) This method should not be preferred, tests needs should not impact software architecture
#[actix_web::test]
async fn test_ensure_account_limit_is_not_reached() {
    // setup
    let (pool, repos, services) = crate::test::setup().await;
    // arrange
    let nb_of_users_to_insert_before_failure = super::MAX_NB_OF_ACCOUNTS as usize - crate::test::get_users().len();
    // act
    for i in 0..nb_of_users_to_insert_before_failure {
        sqlx
            ::query!(
                r#"insert into t_user(id, email, normalized_email, date_creation, password_hash, is_active) values ($1, $2, $2, now(), $3, false);"#,
                uuid::Uuid::new_v4(),
                format!("test_user_{}@domain.com", i),
                format!("password_hash_{}", 10000 + i))
            .execute(pool.as_ref())
            .await
            .unwrap();
    }
    // assert
    let user_to_insert = super::ConfirmRegistrationPayload {
        email: String::from("test_user@domain.com"),
        password: String::from("my_password"),
    };
    let opt_expected_err = super
        ::route(
            actix_web::web::Data::new(repos),
            actix_web::web::Data::new(services.clone()),
            actix_web::web::Json(user_to_insert.clone()))
        .await;
    assert!(opt_expected_err.is_err());
    let crate::AppError { http_status_code, server_status_code, message } = opt_expected_err.err().unwrap();
    assert!(http_status_code.as_u16() >= 500 && http_status_code.as_u16() < 600);
    assert!(!message.is_empty());
    assert_eq!(server_status_code, crate::ServerStatusCode::MaxNbOfAccountsReached);
    // cleanup
    crate::test::cleanup().await;
}