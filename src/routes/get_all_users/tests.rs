use super::super::UserOutput;

#[actix_web::test]
async fn test_get_all_users() {
    // setup
    let (_, repos, _) = crate::test::setup().await;
    // arrange
    let users = crate::test::get_users()
        .into_iter()
        .map(|x| x.into())
        .collect::<Vec<UserOutput>>();
    // act
    let get_all_users_result = super::route(actix_web::web::Data::new(repos)).await;
    // assert
    assert!(get_all_users_result.is_ok());
    for user in &users {
        let user_output_str = serde_json::to_string(user).unwrap();
        assert!(get_all_users_result.as_ref().unwrap().contains(&user_output_str));
    }
    assert_eq!(get_all_users_result.as_ref().unwrap(), &serde_json::to_string(&users).unwrap());
    // cleanup
    crate::test::cleanup().await;
}
