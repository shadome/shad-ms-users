#[cfg(test)]
mod tests;

pub(super) async fn route(
    repositories: actix_web::web::Data<crate::Repositories>,
) -> Result<String, crate::AppError> {
    let users = repositories
        .user_repository()
        .read(None, None)
        .await?
        .into_iter()
        .map(|x| x.into())
        .collect::<Vec<super::UserOutput>>();
    let users_json = serde_json::to_string(&users)?;
    Ok(users_json)
}
