# Testing another file structure

This code base aims at separating concerns by creating as many sub modules as required. Modules represent a scope, which is meant to expose code to some other part of the application in a controlled fashion. In particular, the module hierarchy is to be conceived in the form of a dependency **tree** in most cases, keeping orthogonal layers (ie., layers which can be access by multiple other layers with different levels of nesting) to a minimum number.

As such, and considering Rust's module restrictions, the `mod.rs` syntax will be used to encourage folder nesting and to avoid duplicating module names (e.g., having a duplicate `my_module.rs` file, containing the code, and a `my_module` folder, containing nested submodules).

Consequently, a module folder will typically contain:
- its `mod.rs` file, exposing at least one non-private function, struct, etc.
- an optional `tests.rs` file, containing tests private to the `mod.rs` code, if relevant (not all layers are meant to be tested, in accordance with the TDD paradigm)
- an optional `mod.drawio` (or excalidraw, etc.) if a diagram is provided to break down the conception of the functionalities provided by `mod.rs`

