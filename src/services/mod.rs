pub(crate) mod crypto;
pub(crate) use crypto::*;
pub(crate) mod emails;
pub(crate) use emails::*;

#[derive(Clone, Debug)]
pub(crate) struct Services {
    crypto_service: CryptoService,
    emails_service: EmailsService,
}

impl Services {
    
    // #[tracing::instrument(skip(secret_key))]
    pub(crate) fn new(secret_key: secrecy::SecretString) -> Self {
        Self {
            crypto_service: CryptoService::new(secret_key),
            emails_service: EmailsService::new(),
        }
    }
    
    pub(crate) fn crypto_service(&self) -> &CryptoService { &self.crypto_service }

    pub(crate) fn emails_service(&self) -> &EmailsService { &self.emails_service }
}