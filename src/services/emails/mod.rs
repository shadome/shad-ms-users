
#[derive(Clone, Debug)]
pub(crate) struct EmailsService {}

impl EmailsService {

    pub(super) fn new() -> Self { Self {} }

}

#[cfg(not(test))]
impl EmailsService {
    
    pub(crate) fn check_and_normalise_email(&self, email_raw: &str) -> Result<String, crate::AppError> {
        // TODO this should call the email microservice configured in the .env file
        return Ok(String::from(email_raw));
        // let email_copy = String::from(email_raw);
        // let result =actix_web::rt
        //     ::spawn(
        //         async move { 
        //             norm_email::Normalizer::new()
        //                 .normalize(email_copy.to_owned().as_str())
        //                 .map_err(|anyhow_err| crate::AppError {
        //                     http_status_code: actix_web::http::StatusCode::BAD_REQUEST,
        //                     server_status_code: crate::ServerStatusCode::InvalidEmailFormat,
        //                     message: anyhow_err.to_string(),
        //                 })
        //                 .and_then(|normalized_email| 
        //                     if mailchecker::is_valid(&normalized_email.normalized_address) { 
        //                         Ok(normalized_email.normalized_address)
        //                     } else {
        //                         let err = crate::AppError {
        //                             http_status_code: actix_web::http::StatusCode::BAD_REQUEST,
        //                             server_status_code: crate::ServerStatusCode::BlacklistedEmail,
        //                             message: String::from("The provided email is invalid or blacklisted"),
        //                         };
        //                         Err(err)
        //                     })
        //         })
        //     .await
        //     .expect("rt::spawn");
        // result
    }
}

#[cfg(test)]
impl EmailsService {
    
    pub(crate) fn check_and_normalise_email(&self, email_raw: &str) -> Result<String, crate::AppError> {
        Ok(String::from(email_raw))
    }

}
