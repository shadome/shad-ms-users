// Note: in case a global setup function is required, check an async version of (std::sync::Once)
// https://stackoverflow.com/questions/58006033/how-to-run-setup-code-before-any-tests-run-in-rust 

#[actix_web::test]
async fn test_setup_cleanup() {
    // arrange, act
    let users = get_users();
    assert_ne!(0, users.len());
    let not_yet_inserted = read_test_users_in_db(None).await;
    setup().await;
    let inserted = read_test_users_in_db(None).await;
    cleanup().await;
    let removed = read_test_users_in_db(None).await;
    // assert
    
    // println!("inserted: {}", inserted.iter().map(|item| item.to_string()).collect::<Vec<String>>().join(", "));
    // println!("after removal: {}", removed.iter().map(|item| item.to_string()).collect::<Vec<String>>().join(", "));
    assert_eq!(0, not_yet_inserted.len());
    assert_eq!(0, removed.len());
    assert_eq!(users, inserted);
    assert_eq!(not_yet_inserted, removed);
    // Note: to display stdout when running tests, run with `cargo test -- --nocapture` (note the isolated `--`, see https://stackoverflow.com/questions/25106554/why-doesnt-println-work-in-rust-unit-tests)
    // println!("inserted: {}", inserted.iter().map(|item| item.to_string()).collect::<Vec<String>>().join(", "));
    // println!("after removal: {}", removed.iter().map(|item| item.to_string()).collect::<Vec<String>>().join(", "));
}

/// Return the users which should have been inserted into the database.
/// 
/// Note that this function does NOT return the users which actually are in the database, it doesn't query it.
#[cfg(test)]
pub(crate) fn get_users() -> Vec<crate::User> {
    vec![
        crate::User {
            id: uuid::Uuid::parse_str("00000000-0000-0000-0000-000000000001").unwrap(),
            email: "email1@gmail.com".into(),
            normalised_email: "email1@gmail.com".into(),
            date_creation: chrono::DateTime::parse_from_str("2023-07-11 15:30:00 +00:00", "%Y-%m-%d %H:%M:%S %z").unwrap().into(),
            password_hash: "password_hash1".into(),
            is_active: true,
            validation_token: Some(uuid::Uuid::parse_str("10000000-0000-0000-0000-000000000001").unwrap()),
            validation_token_expiration_date: Some(Into::<chrono::DateTime<chrono::Utc>>::into(chrono::DateTime::parse_from_str("2023-07-11 15:30:00 +00:00", "%Y-%m-%d %H:%M:%S %z").unwrap()).checked_add_days(chrono::Days::new(1)).unwrap()),
        },
        crate::User {
            id: uuid::Uuid::parse_str("00000000-0000-0000-0000-000000000002").unwrap(),
            email: "email2@gmail.com".into(),
            normalised_email: "email2@gmail.com".into(),
            date_creation: chrono::DateTime::parse_from_str("2023-07-10 15:30:00 +00:00", "%Y-%m-%d %H:%M:%S %z").unwrap().into(),
            password_hash: "password_hash2".into(),
            is_active: true,
            validation_token: Some(uuid::Uuid::parse_str("10000000-0000-0000-0000-000000000002").unwrap()),
            validation_token_expiration_date: Some(Into::<chrono::DateTime<chrono::Utc>>::into(chrono::DateTime::parse_from_str("2023-07-10 15:30:00 +00:00", "%Y-%m-%d %H:%M:%S %z").unwrap()).checked_add_days(chrono::Days::new(1)).unwrap()),
        },
    ]
}

#[cfg(test)]
pub(crate) async fn setup() -> (std::sync::Arc<sqlx::postgres::PgPool>, crate::Repositories, crate::Services) {
    let services = get_services().await;
    let db_pool = std::sync::Arc::new(get_db_pool().await);
    let users = get_users();

    // first, cleanup the test database, in case any former test hasn't been successful (and hence has not cleaned up)
    sqlx::query!(r#"delete from t_user;"#).execute(db_pool.as_ref()).await.unwrap();
    
    // then, insert the new users
    for user in users {
        insert_user(db_pool.clone(), &user).await;
    }
    // try_unwrap is safe: the above treatments are all awaited and are not sharing the given Arc
    (db_pool.clone(), crate::repositories::Repositories::new_arc(db_pool), services)
}

/// Note that this function wipes the database, if it is not intended, call `cargo test` with a docker postgres image dedicated to tests.
#[cfg(test)]
pub(crate) async fn cleanup() {
    let db_pool = std::sync::Arc::new(get_db_pool().await);
    sqlx::query!(r#"delete from t_user;"#).execute(db_pool.as_ref()).await.unwrap();
}

#[cfg(test)]
pub(crate) async fn read_test_users_in_db(user_ids: Option<Vec<uuid::Uuid>>) -> Vec<crate::User> {

    let db_pool = std::sync::Arc::new(get_db_pool().await);
    let user_ids = user_ids.unwrap_or(get_users().iter().map(|x| x.id).collect());
    
    sqlx
        ::query_as!(
            crate::User,
            r#"
                select id, email, normalised_email, date_creation, password_hash, is_active, validation_token, validation_token_expiration_date
                from t_user
                where id = any($1)
            ;"#,
            &user_ids)
        .fetch_all(db_pool.as_ref())
        .await
        .unwrap()
}

#[cfg(test)]
async fn get_db_pool() -> sqlx::Pool<sqlx::Postgres> {
    dotenvy::dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL env variable must be set");
    sqlx::postgres::PgPool::connect(&database_url).await.unwrap()
}

#[cfg(test)]
async fn get_services() -> crate::Services {
    dotenvy::dotenv().ok();
    let secret_key: secrecy::SecretString = std::env::var("SECRET_KEY").expect("SECRET_KEY env variable must be set").into();
    crate::Services::new(secret_key)
}

#[cfg(test)]
async fn insert_user(
    db_pool: std::sync::Arc<sqlx::Pool<sqlx::Postgres>>,
    user: &crate::User,
) {
    sqlx
        ::query!(
            r#"
            insert into t_user(
                id, email, normalised_email, date_creation, password_hash, is_active, validation_token, validation_token_expiration_date
            ) values (
                $1, $2, $3, $4, $5, $6, $7, $8
            );"#,
            user.id,
            user.email,
            user.normalised_email,
            user.date_creation,
            user.password_hash,
            user.is_active,
            user.validation_token,
            user.validation_token_expiration_date,
        )
        .execute(db_pool.as_ref())
        .await
        .unwrap();

}


