use std::error::Error;
use std::fmt;

/* AppError implementation, for simpler error management */

#[repr(u16)]
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub(crate) enum ServerStatusCode {
    // global codes
// Ok = 0,
    TechnicalError = 1,
    // register new user
    MaxNbOfAccountsReached = 101,
    EmailAlreadyExists = 102,
    // get user by email
    ProvidedEmailDoesNotExist = 201,
    // check and normalise email
    InvalidEmailFormat = 1001,
    BlacklistedEmail = 1002,
}

impl std::fmt::Display for ServerStatusCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let val = *self as u16;
        write!(f, "{}", val)
    }
}

#[derive(Debug, serde::Serialize)]
pub(crate) struct AppError {
    /// information message to display in a blank error page
    pub(crate) message: String,
    /// http status code, e.g., 404 for NOT_FOUND, or 200 for OK
    #[serde(skip_serializing)]
    pub(crate) http_status_code: actix_web::http::StatusCode,
    /// exposed status code, similar to the http status code but managed by this microservice
    /// e.g., a code for the error "maximum number of users reached" or "user already exists" in case of a sign up
    /// this status code holds a functional value, i.e., it does not describe technical errors
    #[serde(serialize_with = "serialize_server_status_code")]
    pub(crate) server_status_code: ServerStatusCode,
}

fn serialize_server_status_code<S>(x: &ServerStatusCode, s: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    s.serialize_u16(*x as u16)
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl Error for AppError {}

impl From<AppError> for actix_web::HttpResponse {
    fn from(error: AppError) -> Self {
        let body = serde_json::to_string_pretty(&error).unwrap();
        actix_web::HttpResponseBuilder::new(error.http_status_code)
            .content_type(actix_web::http::header::ContentType::json())
            .body(body)
        }
}

/* From AppError to actix ResponseError, for enabling error propagation syntax ("await?", etc.) in handlers */

impl actix_web::error::ResponseError for AppError {
    fn error_response(&self) -> actix_web::HttpResponse {
        let body = serde_json::to_string_pretty(self).unwrap();
        actix_web::HttpResponseBuilder::new(self.http_status_code)
            .content_type(actix_web::http::header::ContentType::plaintext())
            .body(body)
            
    }
}

/* From everything to AppError, for simpler error management */


impl From<std::io::Error> for AppError {
    fn from(error: std::io::Error) -> Self {
        AppError {
            message: error.to_string(),
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: ServerStatusCode::TechnicalError,
        }
    }
}

impl From<serde_json::Error> for AppError {
    fn from(error: serde_json::Error) -> Self {
        AppError {
            message: error.to_string(),
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: ServerStatusCode::TechnicalError,
        }
    }
}

impl From<argonautica::Error> for AppError {
    fn from(error: argonautica::Error) -> Self {
        AppError {
            message: error.to_string(),
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: ServerStatusCode::TechnicalError,
        }
    }
}

impl From<sqlx::Error> for AppError {
    fn from(error: sqlx::Error) -> Self {
        AppError {
            message: format!("{:?}", error),
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: ServerStatusCode::TechnicalError,
        }
    }
}

impl From<validator::ValidationErrors> for AppError {
    /// careful about semantics: validator uses the `error` dataflow of Result to pass down several errors
    /// so the parameter named error represent in fact errors  
    fn from(error: validator::ValidationErrors) -> Self {
        let message = if error.is_empty() {
            "empty ValidationErrors though we're in the error dataflow, should never happen".into()
        } else {
            // Note: the modelling of the validator crate's errors does not provide a deterministic sorting: the same errors can appear in different orders.
            // The following shenanigans ensure error sorting before serialisation.
            let mut vector = error
                .into_errors()
                .into_iter()
                // only keeping the error keys unless if it a `Field` validation error kind (only one used in the app as of now)
                .map(|err| match err.1 {
                    validator::ValidationErrorsKind::Struct(_) => err.0.into(),
                    validator::ValidationErrorsKind::List(_) => err.0.into(),
                    validator::ValidationErrorsKind::Field(field) => field.iter().filter_map(|f| f.message.as_deref()).collect::<Vec<&str>>().join(", "),
                })
                .collect::<Vec<_>>();
            vector.sort();
            vector.join("\n")
        };
        AppError { 
            message,
            http_status_code: actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            server_status_code: ServerStatusCode::TechnicalError,
        }
    }
}
