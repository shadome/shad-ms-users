#[macro_use]
extern crate validator_derive;

mod repositories;
use repositories::*;
mod routes;
mod services;
use services::*;
mod utils;
use utils::*;

// Keep in sync with the content of the .env file
#[derive(Clone, Debug)]
#[warn(dead_code)]
pub struct Env {
    database_url: secrecy::SecretString,
    host: String,
    port: u16,
    secret_key: secrecy::SecretString,
}

fn init_env_variables() -> Env {
    dotenvy::dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL env variable must be set").into();
    let host = std::env::var("HOST").expect("HOST env variable must be set");
    let port = std::env::var("PORT").expect("PORT env variable must be set").parse().expect("PORT env variable must be a i16");
    let secret_key = std::env::var("SECRET_KEY").expect("SECRET_KEY env variable must be set").into();
    Env { database_url, host, port, secret_key }
}

#[actix_web::main]
async fn main() -> Result<(), crate::AppError> {
    /* initialisation */
    // env variables
    let env = init_env_variables();
    // db connection pools
    let db_pool = sqlx::postgres::PgPool::connect(secrecy::ExposeSecret::expose_secret(&env.database_url)).await?;
    // repository layer
    let repos = repositories::Repositories::new(db_pool);
    // service layer
    let services = Services::new(env.secret_key.clone());
    /* application */
    routes::run_webapp(env, services, repos).await?;
    Ok(())
}
