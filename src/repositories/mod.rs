pub(crate) mod user;
pub(crate) use user::*;

#[derive(Clone, Debug)]
pub(crate) struct Repositories {
    user_repository: UserRepository,
}

impl Repositories {
    
    pub(crate) fn new(pg_pool: sqlx::postgres::PgPool) -> Self {
        let pg_pool = std::sync::Arc::new(pg_pool);
        Self {
            user_repository: UserRepository::new(pg_pool.clone()),
        }
    }
    
    #[cfg(test)]
    /// Variant of `Repositories::new` which lets the user keep a reference to the pool.
    pub(crate) fn new_arc(pg_pool: std::sync::Arc<sqlx::postgres::PgPool>) -> Self {
        Self {
            user_repository: UserRepository::new(pg_pool.clone()),
        }
    }
    
    pub(crate) fn user_repository(&self) -> &UserRepository { &self.user_repository }
}