pub(crate) mod count;
pub(crate) mod create;
pub(crate) mod delete;
pub(crate) mod read;

#[derive(Clone, Debug)]
pub(crate) struct UserRepository {
    pg_pool: std::sync::Arc<sqlx::postgres::PgPool>,
}

impl UserRepository {
    
    pub(super) fn new(pg_pool: std::sync::Arc<sqlx::postgres::PgPool>) -> Self {
        Self { pg_pool: pg_pool }
    }

}

#[derive(Clone, Debug, Validate, PartialEq, PartialOrd)]
pub(crate) struct User {
    pub(crate) id: uuid::Uuid,
    pub(crate) email: String,
    pub(crate) normalised_email: String,
    pub(crate) date_creation: chrono::DateTime<chrono::Utc>,
    pub(crate) password_hash: String,
    pub(crate) is_active: bool,
    pub(crate) validation_token: Option<uuid::Uuid>,
    pub(crate) validation_token_expiration_date: Option<chrono::DateTime<chrono::Utc>>,
}

#[cfg(test)]
impl std::fmt::Display for User {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let validation_token_str = self.validation_token.map(|x| x.to_string()).unwrap_or(String::default());
        let validation_token_expiration_date_str = self.validation_token_expiration_date.map(|x| x.to_string()).unwrap_or(String::default());
        write!(
            f,
r#"{{
    id: "{}",
    email: "{}",
    normalised_email: "{}",
    date_creation: "{}",
    password_hash: "{}",
    is_active: {},
    validation_token: {},
    validation_token_expiration_date: {},
}}"#,
            self.id,
            self.email,
            self.normalised_email,
            self.date_creation,
            self.password_hash,
            self.is_active,
            validation_token_str,
            validation_token_expiration_date_str,
        )
    }
}
