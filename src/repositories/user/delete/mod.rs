
impl super::UserRepository {

    pub(crate) async fn delete(&self, ids: Option<Vec<uuid::Uuid>>) -> Result<(), crate::AppError> {
        sqlx
            ::query!(
                r#"
                    delete from t_user 
                    where $1::uuid[] is null or id = any($1)
                ;"#,
                ids.as_deref())
            .fetch_all(self.pg_pool.as_ref())
            .await?;
        Ok(())
    }

}
