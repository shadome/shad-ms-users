
impl super::UserRepository {

    // TODO: implement a design pattern for optional parameters
    // WHY: adding new optional parameters to such functions in case of a new feature leads to compile time errors in code which shouldn't have to change (and therefore risk regression) for this feature
    // See https://vidify.org/blog/rust-parameters/
    // Idea: for the repository layer, maybe implement a derive macro which creates a struct and its builder pattern out of the box for the User type
    // e.g., `UserReadParams::default().emails(e1, e2).id(single_id)`
    pub(crate) async fn read(
        &self,
        ids: Option<Vec<uuid::Uuid>>,
        normalised_emails: Option<Vec<String>>,
    ) -> Result<Vec<crate::User>, crate::AppError> {
        let result = sqlx
            ::query_as!(
                crate::User,
                r#"
                    select id, email, normalised_email, date_creation, password_hash, is_active, validation_token, validation_token_expiration_date
                    from t_user
                    where 1=1 -- used to align following `and` clauses
                        and ($1::uuid[] is null or id = any($1))
                        and ($2::varchar[] is null or normalised_email = any($2))
                ;"#,
                ids.as_deref(),
                normalised_emails.as_deref())
            .fetch_all(self.pg_pool.as_ref())
            .await?;
        Ok(result)
    }

}
