
pub(crate) struct UserCount { value: i64 }

impl UserCount {
    // the cast is implemented here ASAP: it is a count, therefore the i64 value will always be positive
    pub(crate) fn as_u32(&self) -> u32 { self.value as u32 }
}

impl super::UserRepository {

    pub(crate) async fn count(&self) -> Result<UserCount, crate::AppError> {
        let result = sqlx
            ::query_as!(
                UserCount,
                r#"select count(*) as "value!" from t_user;"#)
            .fetch_one(self.pg_pool.as_ref())
            .await?;
        Ok(result)
    }

}
