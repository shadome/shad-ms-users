
impl super::UserRepository {

    pub(crate) async fn create(
        &self, 
        email: &str,
        normalised_email: &str,
        password_hash: &str,
    ) -> Result<uuid::Uuid, crate::AppError> {
        let id = uuid::Uuid::new_v4();
        let date_creation = chrono::Utc::now();
        let is_active = false;
        let validation_token = Some(uuid::Uuid::new_v4());
        let validation_token_expiration_date = Some(chrono::Utc::now().checked_add_days(chrono::Days::new(1)).unwrap());
        sqlx
            ::query!(
                r#"
                insert into t_user(
                    id, email, normalised_email, date_creation, password_hash, is_active, validation_token, validation_token_expiration_date
                ) values (
                    $1, $2, $3, $4, $5, $6, $7, $8
                );"#,
                id,
                email,
                normalised_email,
                date_creation,
                password_hash,
                is_active,
                validation_token,
                validation_token_expiration_date,
            )
            .execute(self.pg_pool.as_ref())
            .await?;
        Ok(id)
    }

}
