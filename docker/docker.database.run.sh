# Trigger the sudo password prompt at the start of the script
sudo pwd

source .docker.env

docker stop ${DB_CONTAINER} || true

# Note to Docker beginners like myself: 
#   The option `-p <host_port>:<image_port>` exposes and forwards the given port but would still
#   not allow the user to query a running container with an URL such as `localhost:<host_port>`.
#   As far as I understand it, the reason behing this behaviour is that the port is not forwarded
#   towards the host machine but towards docker's "default" host.
#   As a result, the option `-h <host_machine_ip_address>` is required, such as `-h 198.2.5.172`.
#   At this point, `-h localhost` would still fail to achieve what is expected because the network
#   localhost loopback matching this keyword would be docker's and not the host machine.
#   Docker provides another way of abstracting the actual ip address (as specifying an actual ip
#   address would not allow for scaling / deploying to multiple hosts): host.docker.internal
# Note that the location of the data can be found in the VOLUME in the Dockerfile of the image,
#   e.g., https://github.com/docker-library/postgres/blob/25b3034e9b0155c3e71acaf650243e7d12a571c1/15/alpine/Dockerfile
docker run --rm -dit \
    -v `pwd`/../.database:/var/lib/postgresql/data \
    -h host.docker.internal \
    -p 8091:5432 \
    --name ${DB_CONTAINER} \
    --env-file=../.env \
    postgres:15-alpine

sudo chmod -R +r ../.database/
