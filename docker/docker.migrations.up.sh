# Trigger the sudo password prompt at the start of the script
sudo pwd

source ../.env
source .docker.env

# Note to Docker beginners like myself:
#   Running this command is only a way for the developer to avoid having to instal liquibase on its own local machine.
#   The same command could be run without docker if liquibase is already installed on the host (local machine).
#   This is why such commands require the option `--net=host` and not other options such as `-h`, because
#   without the explicit network to use, it would use docker's default, and localhost would not match the actual host.
docker run --rm \
    -v `pwd`/../migrations:/liquibase/changelog/ \
    --net=host \
    liquibase/liquibase:4.19 \
    --changelogFile=changelog.xml \
    --url=jdbc:postgresql://localhost:8091/postgres \
    --driver=org.postgresql.Driver \
    --username=postgres \
    --password=${POSTGRES_PASSWORD} \
    update

sudo chmod -R +r ../.database/

# update-testing-rollback
# https://docs.liquibase.com/commands/update/update-testing-rollback.html
# https://docs.liquibase.com/commands/rollback/rollback-count.html
# rollback-count --count=9999