# TODO those tests should run on a postgresql running image dedicated to tests,
# i.e., independant of data already existing in the database in the developer's environment
# Maybe hard code new postgres image credentials in the test part of this application,
# so that test can run on a *-test image without temporarily stopping the production image

# Notes:
# - should only run on one thread, else side-effects such as database insertions will have conflicting PKs etc.
# - enable the nocapture flag to allow tests to output via `println!()` statements
# - `--quiet`` hide output warnings
cargo test -- --nocapture --test-threads 1 --quiet

